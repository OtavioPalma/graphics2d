/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mytable;

import java.awt.Color;

/**
 *
 * @author avilapm
 */
public interface CommandListener {
    
    public int getCommand();
    public int getSizeEraser();
    public Color getColor();
    public String getMessage();
}
