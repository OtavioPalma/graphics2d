package mytable;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JPanel;

public class MyCanvas extends JPanel implements MouseListener, MouseMotionListener {

    private int index = 0;
    private int index_apagar = 0;
    private Point[] arr = new Point[100000];
    private Point[] apagar = new Point[100000];
    private final CommandListener command;

    public MyCanvas(CommandListener command) {
        setBackground(Color.GRAY);
        System.out.println(((SwingControlDemo) command).getPreferredSize());
        int width = (int) ((SwingControlDemo) command).getPreferredSize().getWidth()-35;
        int height = (int) ((SwingControlDemo) command).getPreferredSize().getHeight()-35;
        setSize(new Dimension(width,height));
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        this.command = command;
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2;
        g2 = (Graphics2D) g;
        switch (command.getCommand()) {
            case CanvasConstants.DESENHO_LIVRE:
                for (int i = 0; i < index - 1; i++) {
                    g.setColor(command.getColor());
                    g.drawLine(arr[i].x, arr[i].y, arr[i + 1].x, arr[i + 1].y);
                }   break;
            case CanvasConstants.APAGAR:
                for (int i = 0; i < index_apagar - 1; i++) {
                    g.clearRect(apagar[i].x, apagar[i].y, command.getSizeEraser(), command.getSizeEraser());
                }   break;
            case CanvasConstants.ESCREVER:
                  g2.drawString (command.getMessage(), arr[index-1].x, arr[index-1].y);
                break;
            default:
                break;
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (command.getCommand() == CanvasConstants.DESENHO_LIVRE || command.getCommand() == CanvasConstants.ESCREVER) {
            setCursor(Cursor.getDefaultCursor());
            arr[index] = new Point(e.getX(), e.getY());
            //System.out.println("Enviar dragged: " + arr[index].getX() + " " + arr[index].getY());
            index++;
        } else {
            setCursor(new Cursor(Cursor.HAND_CURSOR));
            apagar[index_apagar] = new Point(e.getX(), e.getY());
            index_apagar++;
        }

        repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        arr[index] = new Point(e.getX(), e.getY());
        //System.out.println("Enviar pressed: " + arr[index].getX() + " " + arr[index].getY());
        index++;

        repaint();
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {

        this.removeMouseListener(this);
        this.removeMouseMotionListener(this);
        
        //transmite para o server 
        for (int i = 0; i < index - 1; i++) {
            System.out.println(arr[i].x + " " +  arr[i].y + " " +  arr[i + 1].x + " " +  arr[i + 1].y);
        } 
        
        
        //restaura o listener
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        
        //limpa o arraylist
        arr = new Point[100000];
        index = 0;
        
        apagar = new Point[100000];
        index_apagar = 0;

    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }


}
